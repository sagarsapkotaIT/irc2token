package com.sagar.icon.pojo;

public class IRC2Token {

    private final String name;
    private final String symbol;
    private final int decimals;

    private IRC2Token(Builder builder){
        this.name = builder.name;
        this.symbol = builder.symbol;
        this.decimals = builder.decimals;

    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getDecimals() {
        return decimals;
    }

    public static class Builder {
        private String name;
        private String symbol;
        private int decimals;

        public Builder name(String name){
            this.name = name;
            return this;
        }

        public Builder symbol(String symbol){
            this.symbol = symbol;
            return this;
        }

        public Builder decimals(int decimals){
            this.decimals = decimals;
            return this;
        }

        public IRC2Token build(){
            return new IRC2Token(this);
        }

    }

}
