package com.sagar.icon;

import com.sagar.icon.pojo.IRC2Token;
import score.Address;
import score.Context;
import score.DictDB;
import score.VarDB;
import score.annotation.External;

import java.math.BigInteger;

public class IRC2BMToken {

    protected static final Address ZERO_ADDRESS = new Address(new byte[Address.LENGTH]);
    private final VarDB<BigInteger> totalSupply = Context.newVarDB("totalSupply", BigInteger.class);
    private final DictDB<Address, BigInteger> balances = Context.newDictDB("balances", BigInteger.class);
    private final IRC2Token token;
    private final VarDB<Address> minter = Context.newVarDB("minter", Address.class);

    public IRC2BMToken(IRC2Token token, BigInteger _initialSupply){
        this.token = token;

        // decimals must be larger than 0 and less than 21
        Context.require(this.token.getDecimals() >= 0 && this.token.getDecimals() <= 21);

        if (minter.get() == null) {
            minter.set(Context.getOwner());
        }

        // mint the initial token supply here
        Context.require(_initialSupply.compareTo(BigInteger.ZERO) >= 0);
        mint(Context.getCaller(), _initialSupply.multiply(pow10(this.token.getDecimals())));
    }

    @External(readonly=true)
    public String name() {
        return token.getName();
    }

    @External(readonly=true)
    public String symbol() {
        return token.getSymbol();
    }

    @External(readonly=true)
    public int decimals() {
        return token.getDecimals();
    }

    @External
    public BigInteger totalSupply() {
        return totalSupply.getOrDefault(BigInteger.ZERO);
    }


    @External
    public BigInteger balanceOf(Address _owner) {
        return safeGetBalance(_owner);
    }

    @External
    public void transfer(Address _to, BigInteger _value, byte[] _data) {
        Address _from = Context.getCaller();

        // check some basic requirements
        Context.require(_value.compareTo(BigInteger.ZERO) >= 0 && safeGetBalance(_from).compareTo(_value) >= 0);

        // adjust the balances
        safeSetBalance(_from, safeGetBalance(_from).subtract(_value));
        safeSetBalance(_to, safeGetBalance(_to).add(_value));

        // if the recipient is SCORE, call 'tokenFallback' to handle further operation
        byte[] dataBytes = (_data == null) ? new byte[0] : _data;
        if (_to.isContract()) {
            Context.call(_to, "tokenFallback", _from, _value, dataBytes);
        }

        // emit Transfer event
        Transfer(_from, _to, _value, dataBytes);
    }

    @External
    public void Transfer(Address _from, Address _to, BigInteger _value, byte[] _data) {}

    @External
    public void tokenFallback(Address _from, BigInteger _value, byte[] _data) {

    }

    /**
     * Creates `amount` tokens and assigns them to `owner`, increasing the total supply.
     */
    @External
    public void mint(Address owner, BigInteger amount) {
        Context.require(!ZERO_ADDRESS.equals(owner) && amount.compareTo(BigInteger.ZERO) >= 0);
        Context.require(Context.getCaller().equals(minter.get()));
        totalSupply.set(totalSupply.getOrDefault(BigInteger.ZERO).add(amount));
        safeSetBalance(owner, safeGetBalance(owner).add(amount));
        Transfer(ZERO_ADDRESS, owner, amount, "mint".getBytes());
    }



    /**
     * Destroys `amount` tokens from `owner`, reducing the total supply.
     */
    @External
    public void burn(BigInteger amount) {
        Address owner = Context.getCaller();
        Context.require(!ZERO_ADDRESS.equals(owner) && amount.compareTo(BigInteger.ZERO) >= 0 && safeGetBalance(owner).compareTo(amount) >= 0);

        safeSetBalance(owner, safeGetBalance(owner).subtract(amount));
        totalSupply.set(totalSupply.getOrDefault(BigInteger.ZERO).subtract(amount));
        Transfer(owner, ZERO_ADDRESS, amount, "burn".getBytes());
    }

    /**
     * Creates _amount number of tokens, and assigns to _account.
     * Increases the balance of that account and the total supply.
     */
    @External
    public void mintTo(Address _account, BigInteger _amount) {
        // simple access control - only the minter can mint new token
        Context.require(Context.getCaller().equals(minter.get()));
        mint(_account, _amount);
    }

    @External
    public void setMinter(Address _minter) {
        // simple access control - only the contract owner can set new minter
        Context.require(Context.getCaller().equals(Context.getOwner()));
        minter.set(_minter);
    }

    private BigInteger safeGetBalance(Address owner) {
        return balances.getOrDefault(owner, BigInteger.ZERO);
    }

    private void safeSetBalance(Address owner, BigInteger amount) {
        balances.set(owner, amount);
    }

    private static BigInteger pow10(int exponent) {
        BigInteger result = BigInteger.ONE;
        for (int i = 0; i < exponent; i++) {
            result = result.multiply(BigInteger.TEN);
        }
        return result;
    }
}
